# Generated by Django 5.0.2 on 2024-02-24 07:54

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Interest",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=256)),
                ("icon", models.ImageField(upload_to="interest_icons/")),
                ("ttf_icon", models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name="Pair",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("participant_1_telegram_id", models.CharField(max_length=1024)),
                ("participant_2_telegram_id", models.CharField(max_length=1024)),
            ],
        ),
        migrations.CreateModel(
            name="Profile",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("dt_created", models.DateTimeField(auto_now_add=True)),
                ("dt_updated", models.DateTimeField(auto_now_add=True)),
                ("name", models.CharField(max_length=256)),
                ("telegram_id", models.CharField(max_length=1024)),
                ("age", models.PositiveIntegerField(default=16)),
                ("about", models.CharField(blank=True, max_length=512)),
                ("town", models.CharField(blank=True, max_length=256)),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.CreateModel(
            name="ZodiacSign",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=256)),
                ("ttf_icon", models.CharField(max_length=32)),
                ("icon", models.ImageField(upload_to="zodiac_sings_icons/")),
            ],
        ),
        migrations.CreateModel(
            name="Photo",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("file_path", models.ImageField(upload_to="profile_videos/")),
                (
                    "profile",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="photos",
                        to="meet.profile",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Like",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "like_profile",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="likes",
                        to="meet.profile",
                    ),
                ),
                (
                    "liked_profile",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="meet.profile"
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Video",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("file_path", models.ImageField(upload_to="profile_images/")),
                (
                    "profile",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="videos",
                        to="meet.profile",
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="profile",
            name="zodiac_sign",
            field=models.OneToOneField(
                on_delete=django.db.models.deletion.CASCADE, to="meet.zodiacsign"
            ),
        ),
    ]
