from django.db import models

from common_utils.mixins import CreateUpdateDateTimeMixin


class ZodiacSign(models.Model):
    name = models.CharField(max_length=256)
    ttf_icon = models.CharField(max_length=32)
    icon = models.ImageField(upload_to="zodiac_sings_icons/")

    def __str__(self):
        return f"{self.name} {self.ttf_icon}"


class Profile(CreateUpdateDateTimeMixin):
    name = models.CharField(max_length=256)
    telegram_id = models.CharField(max_length=1024)
    age = models.PositiveIntegerField(default=16)
    about = models.CharField(max_length=512, blank=True)
    town = models.CharField(max_length=256, blank=True)
    zodiac_sign = models.OneToOneField(ZodiacSign, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name}({self.telegram_id})"


class Video(models.Model):
    file_path = models.ImageField(upload_to="profile_images/")
    profile = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name="videos"
    )

    def __str__(self):
        return f"Video({self.file_path}) for {self.profile}"


class Photo(models.Model):
    file_path = models.ImageField(upload_to="profile_videos/")
    profile = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name="photos"
    )

    def __str__(self):
        return f"Photo({self.file_path}) for {self.profile}"


class Interest(models.Model):
    name = models.CharField(max_length=256)
    icon = models.ImageField(upload_to="interest_icons/")
    ttf_icon = models.CharField(max_length=32)

    def __str__(self):
        return f"{self.name} {self.ttf_icon}"


class Like(models.Model):
    like_profile = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name="likes"
    )
    liked_profile = models.ForeignKey(Profile, on_delete=models.CASCADE)

    def __str__(self):
        return f"Like {self.like_profile} -> {self.liked_profile}"


class Pair(models.Model):
    participant_1_telegram_id = models.CharField(max_length=1024)
    participant_2_telegram_id = models.CharField(max_length=1024)

    def __str__(self):
        return (
            f"Pair {self.participant_1_telegram_id} - {self.participant_2_telegram_id}"
        )
