from django.contrib import admin

from meet.models import Interest, Like, Pair, Photo, Profile, Video, ZodiacSign


@admin.register(Profile)
class ProfileAdminModel(admin.ModelAdmin):
    list_display = ("name", "telegram_id", "age", "town", "zodiac_sign")
    readonly_fields = (
        "dt_created",
        "dt_updated",
    )


@admin.register(Like)
class LikeAdminModel(admin.ModelAdmin):
    list_display = (
        "like_profile",
        "liked_profile",
    )


@admin.register(Pair)
class PairAdminModel(admin.ModelAdmin):
    list_display = (
        "participant_1_telegram_id",
        "participant_2_telegram_id",
    )


@admin.register(Interest)
class InterestAdminModel(admin.ModelAdmin):
    list_display = (
        "name",
        "icon",
        "ttf_icon",
    )


admin.site.register([Photo, Video, ZodiacSign])
