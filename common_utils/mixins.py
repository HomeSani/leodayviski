from django.db import models


class CreateUpdateDateTimeMixin(models.Model):
    """Миксин устанавливающий дату создания и обновления в моделях"""

    dt_created = models.DateTimeField(auto_now_add=True)
    dt_updated = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True
